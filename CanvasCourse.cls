public class CanvasCourse {

	public Integer id;
	public Integer account_id;
	public Integer root_account_id;
	public Integer enrollment_term_id;
	public Integer grading_standard_id;
	public Integer total_students;
	public Integer needs_grading_count;
	public Integer storage_quota_mb;
	public Integer storage_quota_used_mb;
	public Object sis_course_id;
	public Object integration_id;
	public Object enrollments;
	public Object calendar;
	public Object term;
	public Object course_progress;
	public Object permissions;
	public String name;
	public String course_code;
	public String workflow_state;
	public String start_at;
	public String end_at;
	public String default_view;
	public String syllabus_body;
	public String public_description;
	public String license;
	public String course_format;
	public Boolean apply_assignment_group_weights;
	public Boolean is_public;
	public Boolean is_public_to_auth_users;
	public Boolean public_syllabus;
	public Boolean hide_final_grades;
	public Boolean allow_student_assignment_edits;
	public Boolean allow_wiki_comments;
	public Boolean allow_student_forum_attachments;
	public Boolean open_enrollment;
	public Boolean self_enrollment;
	public Boolean restrict_enrollments_to_course_dates;
	public Boolean access_restricted_by_date;

	private Set<String> fields = new Set<String> {
		'id', 'account_id', 'root_account_id', 'enrollment_term_id', 'grading_standard_id', 'total_students', 'needs_grading_count', 'storage_quota_mb', 'storage_quota_used_mb', 'sis_course_id', 'integration_id', 'enrollments', 'calendar', 'term', 'course_progress', 'permissions', 'name', 'course_code', 'workflow_state', 'start_at', 'end_at', 'default_view', 'syllabus_body', 'public_description', 'license', 'course_format', 'apply_assignment_group_weights', 'is_public', 'is_public_to_auth_users', 'public_syllabus', 'hide_final_grades', 'allow_student_assignment_edits', 'allow_wiki_comments', 'allow_student_forum_attachments', 'open_enrollment', 'self_enrollment', 'restrict_enrollments_to_course_dates', 'access_restricted_by_date'
	};
	public Object set(String fieldName, Object value) {
		if (value instanceof Integer) {
			return set(fieldName, (Integer) value);
		} else if (value instanceof Object) {
			return set(fieldName, (Object) value);
		} else if (value instanceof String) {
			return set(fieldName, (String) value);
		} else if (value instanceof Boolean) {
			return set(fieldName, (Boolean) value);
		}
		return null;
	}

	public Integer set(String fieldName, Integer value) {
		if (fieldName == 'id') {
			return id = value;
		} else if (fieldName == 'account_id') {
			return account_id = value;
		} else if (fieldName == 'root_account_id') {
			return root_account_id = value;
		} else if (fieldName == 'enrollment_term_id') {
			return enrollment_term_id = value;
		} else if (fieldName == 'grading_standard_id') {
			return grading_standard_id = value;
		} else if (fieldName == 'total_students') {
			return total_students = value;
		} else if (fieldName == 'needs_grading_count') {
			return needs_grading_count = value;
		} else if (fieldName == 'storage_quota_mb') {
			return storage_quota_mb = value;
		} else if (fieldName == 'storage_quota_used_mb') {
			return storage_quota_used_mb = value;
		}
		return null;
	}

	public Object set(String fieldName, Object value) {
		if (fieldName == 'sis_course_id') {
			return sis_course_id = value;
		} else if (fieldName == 'integration_id') {
			return integration_id = value;
		} else if (fieldName == 'enrollments') {
			return enrollments = value;
		} else if (fieldName == 'calendar') {
			return calendar = value;
		} else if (fieldName == 'term') {
			return term = value;
		} else if (fieldName == 'course_progress') {
			return course_progress = value;
		} else if (fieldName == 'permissions') {
			return permissions = value;
		}
		return null;
	}

	public String set(String fieldName, String value) {
		if (fieldName == 'name') {
			return name = value;
		} else if (fieldName == 'course_code') {
			return course_code = value;
		} else if (fieldName == 'workflow_state') {
			return workflow_state = value;
		} else if (fieldName == 'start_at') {
			return start_at = value;
		} else if (fieldName == 'end_at') {
			return end_at = value;
		} else if (fieldName == 'default_view') {
			return default_view = value;
		} else if (fieldName == 'syllabus_body') {
			return syllabus_body = value;
		} else if (fieldName == 'public_description') {
			return public_description = value;
		} else if (fieldName == 'license') {
			return license = value;
		} else if (fieldName == 'course_format') {
			return course_format = value;
		}
		return null;
	}

	public Boolean set(String fieldName, Boolean value) {
		if (fieldName == 'apply_assignment_group_weights') {
			return apply_assignment_group_weights = value;
		} else if (fieldName == 'is_public') {
			return is_public = value;
		} else if (fieldName == 'is_public_to_auth_users') {
			return is_public_to_auth_users = value;
		} else if (fieldName == 'public_syllabus') {
			return public_syllabus = value;
		} else if (fieldName == 'hide_final_grades') {
			return hide_final_grades = value;
		} else if (fieldName == 'allow_student_assignment_edits') {
			return allow_student_assignment_edits = value;
		} else if (fieldName == 'allow_wiki_comments') {
			return allow_wiki_comments = value;
		} else if (fieldName == 'allow_student_forum_attachments') {
			return allow_student_forum_attachments = value;
		} else if (fieldName == 'open_enrollment') {
			return open_enrollment = value;
		} else if (fieldName == 'self_enrollment') {
			return self_enrollment = value;
		} else if (fieldName == 'restrict_enrollments_to_course_dates') {
			return restrict_enrollments_to_course_dates = value;
		} else if (fieldName == 'access_restricted_by_date') {
			return access_restricted_by_date = value;
		}
		return null;
	}

}
