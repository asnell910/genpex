'use strict'

var fs = require('fs'),
  writeCallback = require('./writeCallback'),
  jsonFile = 'course_object.json',
  className = 'CanvasCourse';

var jsToApexTypes = {
  "string": "String",
  "object": "Object",
  "boolean": "Boolean"
}

var properties = {}

function typeOf(obj) {
  var jsType = typeof obj;
  if (jsType === 'number') {
    return Number.isInteger(obj) ? 'Integer' : 'Double';
  } else if (jsToApexTypes.hasOwnProperty(jsType)) {
    return jsToApexTypes[jsType];
  }
  throw 'Found unrecognized type [' + type + '] for value: ' + obj;
}

console.log('Opening ' + jsonFile + '...');
var json = JSON.parse(fs.readFileSync(jsonFile));

var newFile = 'public class ' + className + ' {\n\n';

for (var prop in json) {
  var val = json[prop];
  var type = typeOf(val);
  if (properties[type] == undefined) {
    properties[type] = [prop];
  } else {
    properties[type].push(prop);
  }
}

for (var type in properties) {
  for (var val of properties[type]) {
    newFile += '\tpublic ' + type + ' ' + val + ';\n';
  }
}

newFile += '\n\tprivate Set<String> fields = new Set<String> {';
var first = true;
for (var type in properties) {
  for (var val of properties[type]) {
    newFile += first ? '\n\t\t' : ', ';
    first = false;
    newFile += '\'' + val + '\''
  }
}
newFile += '\n\t};\n';

newFile += '\n\tpublic Object set(String fieldName, Object value) {\n';
var first = true;
for (var type in properties) {
  newFile += first ? '\t\t' : ' else ';
  first = false;
  newFile += 'if (value instanceof ' + type + ') {\n\t\t\t'
    + 'return set(fieldName, (' + type + ') value);\n\t\t}';
}
newFile += '\n\t\treturn null;\n\t}\n';

for (var type in properties) {
  newFile += '\n\tpublic ' + type + ' set(String fieldName, ' + type + ' value) {\n';
  var first = true;
  for (let val of properties[type]) {
    newFile += first ? '\t\t' : ' else ';
    first = false;
    newFile += 'if (fieldName == \'' + val + '\') {\n\t\t\t'
      + 'return ' + val + ' = value;\n\t\t}';
  }
  newFile += '\n\t\treturn null;\n\t}\n';
}

newFile += '\n}\n';

fs.writeFile(className + '.cls', newFile, writeCallback);
