/**
 * Callback function for fs.appendWrite
 */

module.exports = function (err) {
  if (err) {
    throw err;
  }
};
